/* WiFi station Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"
#include "mqtt_client.h"

#include "driver/gpio.h"
#include "dht.h"

/*
   Just change the below entries to strings with
   the config you want - ie #define EXAMPLE_WIFI_SSID "mywifissid"
*/

#define EXAMPLE_ESP_WIFI_SSID      "M T G"  // Wifi Name
#define EXAMPLE_ESP_WIFI_PASS      "Owee/raaa" // Wifi password
#define EXAMPLE_ESP_MAXIMUM_RETRY  4 

#define STACK_SIZE 1024 * 2
#define BLINK_GPIO 2

static const dht_sensor_type_t sensor_type = DHT_TYPE_DHT11;
#if defined(CONFIG_IDF_TARGET_ESP8266)
static const gpio_num_t dht_gpio = 4;
#else
static const gpio_num_t dht_gpio = 5;
#endif

// ******* MQTT,Thingspeak credentials ***********

#define MQTT_HOST_URL      "mqtt.thingspeak.com"
#define MQTT_API_KEY       "08R160SW5L7WTMVF" 
#define MQTT_WRITE_API_KEY "9LFVHHUNXFQHKQCZ"
#define MQTT_READ_API_KEY  "C90BL6U0TTTYHFJW"
#define MQTT_USER_NAME     "mwa0000022919058"
#define MQTT_CHANEL_ID     "1427415"

static const char *TAG = "White walkers";

/* FreeRTOS event group to signal when we are connected*/
static EventGroupHandle_t s_wifi_event_group;
esp_mqtt_client_handle_t gl_client = NULL;


/* The event group allows multiple bits for each event, but we only care about two events:
 * - we are connected to the AP with an IP
 * - we failed to connect after the maximum amount of retries */
#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT      BIT1

static int s_retry_num = 0;

TaskHandle_t ledTaskHandle = NULL;
TaskHandle_t dhtTaskHandle = NULL;

void ledTaskOnPublish(void *param) {
uint8_t count = 0;
while (true)
    {
        gpio_set_level(BLINK_GPIO,1);
        vTaskDelay(100 / portTICK_PERIOD_MS);
        gpio_set_level(BLINK_GPIO,0);
        vTaskDelay(100 / portTICK_PERIOD_MS);
        count++;
        if(count == 6) break;
    }
    vTaskDelete(NULL);
    ledTaskHandle = NULL;
}

// ************************** DHT Sensor ************************************

void dht_test(void *pvParameters)
{
    int16_t temperature = 0;
    int16_t humidity = 0;
    int16_t lastTemp = 0;
    int16_t lastHum = 0;

    // DHT sensors that come mounted on a PCB generally have
    // pull-up resistors on the data pin.  It is recommended
    // to provide an external pull-up resistor otherwise...

    gpio_set_pull_mode(dht_gpio, GPIO_PULLUP_ONLY);

    while (true)
    {
        if (dht_read_data(sensor_type, dht_gpio, &humidity, &temperature) == ESP_OK) {
            printf("Humidity: %d%% Temp: %dC\n", humidity / 10, temperature / 10);

            humidity = humidity / 10;
            temperature = temperature / 10;

            if(lastTemp != temperature && gl_client != NULL) {
               char topic[60];
               sprintf(topic,"channels/%s/publish/fields/field1/%s",MQTT_CHANEL_ID,MQTT_WRITE_API_KEY); 
               ESP_LOGI(TAG,"Topic url is %s",topic);
               char payload[5];
               itoa(temperature, payload, 10);
               int msg_id = esp_mqtt_client_publish(gl_client,topic,payload, 0, 0, 0);
               ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);            
               lastTemp = temperature;
               xTaskCreate(ledTaskOnPublish,"led_task",STACK_SIZE,NULL,2,NULL); 
               }
                
              if(lastHum != humidity && gl_client != NULL) {
                char topic[60];
                sprintf(topic,"channels/%s/publish/fields/field2/%s",MQTT_CHANEL_ID,MQTT_WRITE_API_KEY); 
                ESP_LOGI(TAG,"Topic url is %s",topic);
                char payload[5];
                itoa(humidity, payload, 10);
                int msg_id = esp_mqtt_client_publish(gl_client,topic,payload, 0, 0, 0);
                ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);    
                lastHum = humidity;
                xTaskCreate(ledTaskOnPublish,"led_task",STACK_SIZE,NULL,2,NULL); 
            }
        }
        else
            printf("Could not read data from sensor\n");

        vTaskDelay(pdMS_TO_TICKS(3000));
    }
}

// *************************************** MQTT Protocol ******************************

static esp_err_t mqtt_event_handler_cb(esp_mqtt_event_handle_t event)
{
    esp_mqtt_client_handle_t client = event->client;
    if(gl_client != NULL) gl_client = NULL;
    gl_client = client;
    int msg_id = 0;
    // your_context_t *context = event->context;
    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
            
            //char payload[12];
           // char topic[60];
           // sprintf(topic,"channels/%s/publish/fields/field1/%s",MQTT_CHANEL_ID,MQTT_WRITE_API_KEY); 
            //sprintf(payload,"field1=%d",80);
           // ESP_LOGI(TAG,"Topic url is %s",topic);
           // ESP_LOGI(TAG,"Payload is %s",payload);
           // msg_id = esp_mqtt_client_publish(client,topic,"0", 0, 0, 0);
           // ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
           // xTaskCreate(ledTaskOnPublish,"led_task",STACK_SIZE,NULL,2,ledTaskHandle);  /////////////////////// .   Need handles
            xTaskCreate(dht_test, "dht_task", configMINIMAL_STACK_SIZE * 3, NULL, 5, dhtTaskHandle); 

           // msg_id = esp_mqtt_client_subscribe(client, "/topic/qos0", 0);
           // ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);

           // msg_id = esp_mqtt_client_subscribe(client, "/topic/qos1", 1);
           // ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);

           // msg_id = esp_mqtt_client_unsubscribe(client, "/topic/qos1");
           // ESP_LOGI(TAG, "sent unsubscribe successful, msg_id=%d", msg_id);
            break;
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
            if(dhtTaskHandle != NULL) {
                vTaskDelete(dhtTaskHandle);
                dhtTaskHandle = NULL;
            }
            break;

        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED");
           // msg_id = esp_mqtt_client_publish(client, "/topic/qos0", "data", 0, 0, 0);
           // ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
           /* Used to pass out the created task's handle. */

            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            xTaskCreate(ledTaskOnPublish,"led_task",STACK_SIZE,NULL,2,NULL); 
            break;
        case MQTT_EVENT_DATA:
            ESP_LOGI(TAG, "MQTT_EVENT_DATA");
            printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
            printf("DATA=%.*s\r\n", event->data_len, event->data);
            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            break;
        default:
            ESP_LOGI(TAG, "Other event id:%d", event->event_id);
            break;
    }
    return ESP_OK;
}

static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    mqtt_event_handler_cb(event_data);
}

static void mqtt_app_start(void)
{
    esp_mqtt_client_config_t mqtt_cfg = {
        .host = MQTT_HOST_URL,
        .port = 1883,
        .username = MQTT_USER_NAME,
        .password =  MQTT_API_KEY,
        //CONFIG_BROKER_URL,
    };
#if CONFIG_BROKER_URL_FROM_STDIN
    char line[128];

    if (strcmp(mqtt_cfg.uri, "FROM_STDIN") == 0) {
        int count = 0;
        printf("Please enter url of mqtt broker\n");
        while (count < 128) {
            int c = fgetc(stdin);
            if (c == '\n') {
                line[count] = '\0';
                break;
            } else if (c > 0 && c < 127) {
                line[count] = c;
                ++count;
            }
            vTaskDelay(10 / portTICK_PERIOD_MS);
        }
        mqtt_cfg.uri = line;
        printf("Broker url: %s\n", line);
    } else {
        ESP_LOGE(TAG, "Configuration mismatch: wrong broker url");
        abort();
    }
#endif /* CONFIG_BROKER_URL_FROM_STDIN */

    esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, client);
    esp_mqtt_client_start(client);
}

// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& Station connection section **************************

static void event_handler(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        esp_wifi_connect();
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
        if (s_retry_num < EXAMPLE_ESP_MAXIMUM_RETRY) {
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI(TAG, "retry to connect to the AP");
        } else {
            xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
        }
        ESP_LOGI(TAG,"connect to the AP fail");
    } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    }
}

void wifi_init_sta(void)
{
    s_wifi_event_group = xEventGroupCreate();

    ESP_ERROR_CHECK(esp_netif_init());

    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_create_default_wifi_sta();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    esp_event_handler_instance_t instance_any_id;
    esp_event_handler_instance_t instance_got_ip;
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &event_handler,
                                                        NULL,
                                                        &instance_any_id));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                        IP_EVENT_STA_GOT_IP,
                                                        &event_handler,
                                                        NULL,
                                                        &instance_got_ip));

    wifi_config_t wifi_config = {
        .sta = {
            .ssid = EXAMPLE_ESP_WIFI_SSID,
            .password = EXAMPLE_ESP_WIFI_PASS,
            /* Setting a password implies station will connect to all security modes including WEP/WPA.
             * However these modes are deprecated and not advisable to be used. Incase your Access point
             * doesn't support WPA2, these mode can be enabled by commenting below line */
	     .threshold.authmode = WIFI_AUTH_WPA2_PSK,

            .pmf_cfg = {
                .capable = true,
                .required = false
            },
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );

    ESP_LOGI(TAG, "wifi_init_sta finished.");

    /* Waiting until either the connection is established (WIFI_CONNECTED_BIT) or connection failed for the maximum
     * number of re-tries (WIFI_FAIL_BIT). The bits are set by event_handler() (see above) */
    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
            WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
            pdFALSE,
            pdFALSE,
            portMAX_DELAY);

    /* xEventGroupWaitBits() returns the bits before the call returned, hence we can test which event actually
     * happened. */
    if (bits & WIFI_CONNECTED_BIT) {
        ESP_LOGI(TAG, "connected to ap SSID:%s password:%s",
                 EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
                 ESP_LOGI(TAG,"Starting mqtt_app");
                 mqtt_app_start();
    } else if (bits & WIFI_FAIL_BIT) {
        ESP_LOGI(TAG, "Failed to connect to SSID:%s, password:%s",
                 EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
    } else {
        ESP_LOGE(TAG, "UNEXPECTED EVENT");
    }

    /* The event will not be processed after unregister */
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, instance_got_ip));
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(WIFI_EVENT, ESP_EVENT_ANY_ID, instance_any_id));
    vEventGroupDelete(s_wifi_event_group);
}

void app_main(void)
{ 
    // Just for info
    ESP_LOGI(TAG, "[APP] Startup..");
    ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

    // For onbarod led 
    gpio_pad_select_gpio(BLINK_GPIO);
    /* Set the GPIO as a push/pull output */
    gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);

    //Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    ESP_ERROR_CHECK(esp_netif_init());
   // ESP_ERROR_CHECK(esp_event_loop_create_default());

    ESP_LOGI(TAG, "ESP_WIFI_MODE_STA");
    wifi_init_sta();
    
}
